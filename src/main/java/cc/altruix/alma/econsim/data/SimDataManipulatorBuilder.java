package cc.altruix.alma.econsim.data;

import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.util.persistence.InvalidDataException;

import java.util.Optional;

/**
 * Created by pisarenko on 31.03.2016.
 */
public class SimDataManipulatorBuilder implements DataManipulatorBuilder<SimData, ImmutableSimData> {
    @Override
    public SimData create() {
        return new SimData();
    }

    @Override
    public Optional<SimData> createFrom(DataHolder dataHolder) {
        return Optional.of(dataHolder.get(SimData.class).orElse(new SimData()));
    }

    @Override
    public Optional<SimData> build(final DataView container) throws InvalidDataException {
        if (container.contains(SimKeys.DAYS_SINCE_LAST_MEAL.getQuery())) {
            final int daysSinceLastMeal = container.getInt(SimKeys.DAYS_SINCE_LAST_MEAL.getQuery()).get();
            final String name = container.getString(SimKeys.NAME.getQuery()).get();
            return Optional.of(new SimData(daysSinceLastMeal, name));
        }
        return Optional.empty();
    }
}
