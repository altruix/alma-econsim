package cc.altruix.alma.econsim.data;

import com.google.common.collect.ComparisonChain;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.MemoryDataContainer;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableData;
import org.spongepowered.api.data.value.BaseValue;
import org.spongepowered.api.data.value.immutable.ImmutableValue;

import java.util.Optional;

/**
 * Created by pisarenko on 31.03.2016.
 */
public class ImmutableSimData extends AbstractImmutableData<ImmutableSimData, SimData> {
    private final int daysSinceLastMeal;
    private final String name;

    public ImmutableSimData() {
        this(0, "");
    }

    public ImmutableSimData(final int adays, final String aname) {
        this.daysSinceLastMeal = adays;
        this.name = aname;
    }
    public ImmutableValue<Integer> daysSinceLastMeal() {
        return Sponge.getRegistry().getValueFactory().createBoundedValueBuilder(SimKeys.DAYS_SINCE_LAST_MEAL)
                .defaultValue(0)
                .minimum(0)
                .maximum(30)
                .actualValue(this.daysSinceLastMeal)
                .build()
                .asImmutable();
    }
    public ImmutableValue<String> name() {
        return Sponge.getRegistry().getValueFactory().createValue(SimKeys.NAME, "", this.name).asImmutable();
    }
    @Override
    protected void registerGetters() {
        registerGetDaysSinceLastMeal();
        registerName();
    }

    protected void registerName() {
        registerFieldGetter(SimKeys.NAME, this::getName);
        registerKeyValue(SimKeys.NAME, this::name);
    }

    protected void registerGetDaysSinceLastMeal() {
        registerFieldGetter(SimKeys.DAYS_SINCE_LAST_MEAL, this::getDaysSinceLastMeal);
        registerKeyValue(SimKeys.DAYS_SINCE_LAST_MEAL, this::daysSinceLastMeal);
    }

    @Override
    public <E> Optional<ImmutableSimData> with(Key<? extends BaseValue<E>> key, E value) {
        return Optional.empty();
    }
    @Override
    public SimData asMutable() {
        return new SimData(this.daysSinceLastMeal, this.name);
    }
    @Override
    public int compareTo(final ImmutableSimData obj) {
        return ComparisonChain.start()
                .compare(obj.daysSinceLastMeal, this.daysSinceLastMeal)
                .compare(obj.name, this.name)
                .result();
    }
    @Override
    public int getContentVersion() {
        return 1;
    }
    @Override
    public DataContainer toContainer() {
        return new MemoryDataContainer()
                .set(SimKeys.DAYS_SINCE_LAST_MEAL, this.daysSinceLastMeal)
                .set(SimKeys.NAME, this.name);
    }
    private int getDaysSinceLastMeal() {
        return this.daysSinceLastMeal;
    }
    private String getName() {return this.name;}

}
