package cc.altruix.alma.econsim.data;

import com.google.common.base.Objects;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.MutableBoundedValue;
import org.spongepowered.api.data.value.mutable.Value;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Optional;

/**
 * Created by pisarenko on 31.03.2016.
 */
public class SimData extends AbstractData<SimData, ImmutableSimData> {
    private int daysSinceLastMeal;
    private String name;

    public SimData() {
        this(0, "");
    }

    public SimData(final int defDaysSinceLastMeal, final String aname) {
        this.daysSinceLastMeal = defDaysSinceLastMeal;
        this.name = aname;
    }

    public MutableBoundedValue<Integer> daysSinceLastMeal() {
        return Sponge.getRegistry().getValueFactory().createBoundedValueBuilder(SimKeys.DAYS_SINCE_LAST_MEAL)
                .defaultValue(0)
                .minimum(0)
                .maximum(30)
                .actualValue(this.daysSinceLastMeal)
                .build();
    }

    public Value<String> name() {
        return Sponge.getRegistry().getValueFactory().createValue(SimKeys.NAME, "", this.name);
    }

    @Override
    protected void registerGettersAndSetters() {
        registerDaysSinceLastMeal();
        registerName();
    }

    protected void registerName() {
        registerFieldGetter(SimKeys.NAME, () -> this.name);
        registerFieldSetter(SimKeys.NAME, value -> this.name = checkNotNull(value));
        registerKeyValue(SimKeys.NAME, this::name);
    }

    protected void registerDaysSinceLastMeal() {
        registerFieldGetter(SimKeys.DAYS_SINCE_LAST_MEAL, () -> this.daysSinceLastMeal);
        registerFieldSetter(SimKeys.DAYS_SINCE_LAST_MEAL, value -> this.daysSinceLastMeal = value);
        registerKeyValue(SimKeys.DAYS_SINCE_LAST_MEAL, this::daysSinceLastMeal);
    }

    @Override
    public Optional<SimData> fill(final DataHolder dataHolder, final MergeFunction overlap) {
        return Optional.empty();
    }

    @Override
    public Optional<SimData> from(DataContainer container) {
        if (!container.contains(SimKeys.DAYS_SINCE_LAST_MEAL.getQuery())) {
            return Optional.empty();
        }
        final int daysSinceLastMeal = container.getInt(SimKeys.DAYS_SINCE_LAST_MEAL.getQuery()).get();
        this.daysSinceLastMeal = daysSinceLastMeal;
        return Optional.of(this);
    }

    @Override
    public SimData copy() {
        return new SimData(this.daysSinceLastMeal, this.name);
    }

    @Override
    public ImmutableSimData asImmutable() {
        return new ImmutableSimData(this.daysSinceLastMeal, this.name);
    }

    @Override
    public int compareTo(final SimData obj) {
        return 0;
    }

    @Override
    public int getContentVersion() {
        return 1;
    }

    @Override
    public DataContainer toContainer() {
        return super.toContainer()
                .set(SimKeys.DAYS_SINCE_LAST_MEAL, this.daysSinceLastMeal);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("daysSinceLastMeal", this.daysSinceLastMeal)
                .toString();
    }
}
