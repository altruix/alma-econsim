package cc.altruix.alma.econsim.data;

import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.MutableBoundedValue;
import org.spongepowered.api.data.value.mutable.Value;

import static org.spongepowered.api.data.DataQuery.of;
import static org.spongepowered.api.data.key.KeyFactory.makeSingleKey;
/**
 * Created by pisarenko on 31.03.2016.
 */
public class SimKeys {
    public static final Key<MutableBoundedValue<Integer>> DAYS_SINCE_LAST_MEAL =
            makeSingleKey(Integer.class, MutableBoundedValue.class, of("DaysSinceLastMeal"));
    public static final Key<Value<String>> NAME = makeSingleKey(String.class, Value.class, of("Name"));

}
