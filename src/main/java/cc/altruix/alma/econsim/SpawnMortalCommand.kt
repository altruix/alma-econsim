package cc.altruix.alma.econsim

import org.spongepowered.api.command.CommandResult
import org.spongepowered.api.command.CommandSource
import org.spongepowered.api.command.args.CommandContext
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.entity.living.player.Player

class SpawnMortalCommand (val villagerFactory: VillagerFactory = VillagerFactory()) : CommandExecutor {
    override fun execute(src: CommandSource?, ctx: CommandContext?): CommandResult? {
        if (src is Player) {
            villagerFactory.spawnVillager(src.location, "Villager 1")
        }
        return CommandResult.success()
    }
}