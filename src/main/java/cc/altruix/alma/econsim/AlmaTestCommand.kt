package cc.altruix.alma.econsim

import org.spongepowered.api.command.CommandResult
import org.spongepowered.api.command.CommandSource
import org.spongepowered.api.command.args.CommandContext
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.text.Text

/**
 * Created by pisarenko on 29.03.2016.
 */
class AlmaTestCommand: CommandExecutor {
    override fun execute(src: CommandSource?, ctx: CommandContext?): CommandResult? {
        src?.sendMessage("Altruix Alma is here".toMc());
        return CommandResult.success();
    }
}