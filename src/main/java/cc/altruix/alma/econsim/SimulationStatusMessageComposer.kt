package cc.altruix.alma.econsim

import org.spongepowered.api.block.BlockTypes
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World

/**
 * Created by pisarenko on 01.04.2016.
 */
class SimulationStatusMessageComposer : ISimulationStatusMessageComposer {
    override fun composeStatusMessage(world: World): Array<String> {
        val villager1 = world.findEntityByName(InitSimCommand.villager1Name)
        val villager2 = world.findEntityByName(InitSimCommand.villager2Name)
        val time = world.properties.worldTime
        val daysSinceLastMeal1 = extractDaysSinceLastMeal(villager1)
        val daysSinceLastMeal2 = extractDaysSinceLastMeal(villager1)
        val villager1Alive = (villager1 != null)
        val villager2Alive = (villager2 != null)
        var numberOfFoodItems = 0
        if (villager2 != null) {
            numberOfFoodItems = calculateNumberOfFoodItems(villager2.location, world)
        }

        return arrayOf(
                "Minecraft time (ticks): $time",
                "Villager 1: Days since last meal: $daysSinceLastMeal1, ${boolean2Text(villager1Alive)}",
                "Villager 2: Days since last meal: $daysSinceLastMeal2, ${boolean2Text(villager2Alive)}",
                "Food items: $numberOfFoodItems")
    }

    private fun calculateNumberOfFoodItems(location: Location<World>, world: World): Int {
        val loc3 = createFoodLocation(location, world)
        var foodItems = 0

        for (i in 0..InitSimCommand.initialNumberOfFoodItems) {
            val curLoc = Location<World>(world, loc3.x + i, loc3.y, loc3.z)
            if (BlockTypes.POTATOES.equals(curLoc.blockType)) {
                foodItems++
            }
        }
        return foodItems
    }

    private fun extractDaysSinceLastMeal(villager1: Entity?) = villager1?.getDaysSinceLastMeal() ?: -1
    fun boolean2Text(alive:Boolean):String {
        if (alive) {
            return "alive"
        }
        return "dead"
    }
}