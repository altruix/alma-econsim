package cc.altruix.alma.econsim

import cc.altruix.alma.econsim.data.SimData
import cc.altruix.alma.econsim.data.SimKeys
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.entity.EntityTypes
import org.spongepowered.api.event.cause.Cause
import org.spongepowered.api.event.cause.NamedCause
import org.spongepowered.api.event.cause.entity.spawn.EntitySpawnCause
import org.spongepowered.api.event.cause.entity.spawn.SpawnTypes
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World
import java.util.*

/**
 * Created by pisarenko on 31.03.2016.
 */
class VillagerFactory {
    fun spawnVillager(location: Location<World>, name: String) {
        val extent = location.extent
        val optional = extent.createEntity(EntityTypes.VILLAGER, location.position)
        if (optional.isPresent) {
            val villager = optional.get()
            extent.spawnEntity(
                    villager,
                    Cause.of(NamedCause.source(EntitySpawnCause.builder()
                            .entity(villager).type(SpawnTypes.PLUGIN).build()))
            )
            villager.setDaysSinceLastMeal(0)
            villager.setName(name)
        }
    }
}
