package cc.altruix.alma.econsim

import com.google.inject.Inject
import org.joda.time.Days
import org.joda.time.LocalDateTime
import org.slf4j.Logger
import org.spongepowered.api.Game
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.entity.EntityTypes
import org.spongepowered.api.scheduler.Task
import java.util.*
import java.util.function.Consumer

/**
 * Created by pisarenko on 29.03.2016.
 */
class DeathTask(val game: Game) : Consumer<Task> {
    @Inject lateinit var logger: Logger
    val lastEatingTimesByVillagerUids = HashMap<UUID,LocalDateTime>()
    override fun accept(task: Task?) {
        if (task == null) {
            return
        }
        val worldOpt = game?.server?.getWorld("alma")
        if ((worldOpt == null) || !worldOpt.isPresent) {
            return
        }
        val world = worldOpt.get()
        val villagers = world.entities.filter { x ->
            EntityTypes.VILLAGER.equals(x.type)
        }
        val now = LocalDateTime.now()
        villagers.forEach { villager ->
            val uid = villager.uniqueId
            if (!this.lastEatingTimesByVillagerUids.containsKey(uid)) {
                logger.info("Villager '${villager.uniqueId}' has no info on last eating time")
                lastEatingTimesByVillagerUids.put(uid, now)
            } else {
                val lastEatingTime = lastEatingTimesByVillagerUids.get(uid)
                val days = Days.daysBetween(lastEatingTime, now).days
                logger.info("Villager '${villager.uniqueId}' ate $days ago")
                if (days >= 30) {
                    kill(villager)
                }
            }
        }
    }

    private fun kill(villager: Entity) {
        villager.remove()
        lastEatingTimesByVillagerUids.remove(villager.uniqueId)
        logger.info("Villager died of hunger, UID: '${villager.uniqueId}'")
    }
}