package cc.altruix.alma.econsim

import org.spongepowered.api.command.CommandResult
import org.spongepowered.api.command.CommandSource
import org.spongepowered.api.command.args.CommandContext
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.entity.living.player.Player
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World

/**
 * Created by pisarenko on 01.04.2016.
 */
class SimCommand(val msgComposer: ISimulationStatusMessageComposer = SimulationStatusMessageComposer())
: CommandExecutor {
    companion object {
        const val days = "days"
    }
    override fun execute(src: CommandSource?, ctx: CommandContext?): CommandResult? {
        if ((src is Player) && (ctx != null)) {
            val days = ctx.getOne<Int>(days).get()

            for (i in 0..days) {
                runSimulation(src.world)
            }
            val msg = msgComposer.composeStatusMessage(src.world)
            src.sendMessages(msg.map { x -> x.toMc() })
        }
        return CommandResult.success();
    }

    private fun runSimulation(world: World) {
        val villager1 = world.findEntityByName(InitSimCommand.villager1Name)
        val villager2 = world.findEntityByName(InitSimCommand.villager2Name)

        if (villager1 == null) {
            return
        }
        if (villager2 == null) {
            return
        }

        // Find villager 1
        villager1.incrementDaysSinceLastMeal()

        // Find villager 2
        val locationOfNextFoodItem = findFood(villager2.location, world)
        if (locationOfNextFoodItem != null) {
            // Go to food

            // Go back
        } else {
            villager2.incrementDaysSinceLastMeal()
        }
    }

    private fun findFood(location: Location<World>, world: World): Location<World>? {
        val foodLocation = createFoodLocation(location, world)
        var i = 0
        var locationWithFood = null

        while ((i < InitSimCommand.initialNumberOfFoodItems) && (locationWithFood == null)) {

        }

        return locationWithFood
    }

}