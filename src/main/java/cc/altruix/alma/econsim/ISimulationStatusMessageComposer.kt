package cc.altruix.alma.econsim

import org.spongepowered.api.world.World

/**
 * Created by pisarenko on 01.04.2016.
 */
interface ISimulationStatusMessageComposer {
    fun composeStatusMessage(world: World):Array<String>
}