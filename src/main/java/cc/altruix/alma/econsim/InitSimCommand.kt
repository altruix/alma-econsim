package cc.altruix.alma.econsim

import org.spongepowered.api.block.BlockTypes
import org.spongepowered.api.command.CommandResult
import org.spongepowered.api.command.CommandSource
import org.spongepowered.api.command.args.CommandContext
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.entity.living.player.Player
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World

/**
 * Created by pisarenko on 31.03.2016.
 */
class InitSimCommand(val villagerFactory: VillagerFactory = VillagerFactory(),
                     val msgComposer: ISimulationStatusMessageComposer = SimulationStatusMessageComposer()
    ) : CommandExecutor {
    companion object {
        const val initialNumberOfFoodItems = 10
        const val villager1Name = "Villager 1"
        const val villager2Name = "Villager 2"
    }

    override fun execute(src: CommandSource?, ctx: CommandContext?): CommandResult? {
        if (src is Player) {
            val loc1 = createVillager1Location(src)
            val loc2 = createVillager2Location(src)
            villagerFactory.spawnVillager(loc1, villager1Name)
            villagerFactory.spawnVillager(loc2, villager2Name)
            val loc3 = createFoodLocation(loc2, src.world)
            putFood(loc3, src.world)
            val msg = msgComposer.composeStatusMessage(src.world)
            src.sendMessages(msg.map { x -> x.toMc() })
        }
        return CommandResult.success();
    }

    private fun putFood(loc: Location<World>, world: World) {
        for (i in 0..initialNumberOfFoodItems) {
            val curLoc = Location<World>(world, loc.x + i, loc.y, loc.z)
            curLoc.blockType = BlockTypes.POTATOES
        }
    }

    fun createVillager2Location(src: Player) = Location<World>(
            src.world,
            src.location.x + 2,
            src.location.y,
            src.location.z
    )

    fun createVillager1Location(src: Player) = Location<World>(
            src.world,
            src.location.x,
            src.location.y,
            src.location.z
    )
}
