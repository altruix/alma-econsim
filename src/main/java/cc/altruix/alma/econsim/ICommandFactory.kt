package cc.altruix.alma.econsim

import org.spongepowered.api.command.CommandCallable
import org.spongepowered.api.command.spec.CommandExecutor

/**
 * Created by pisarenko on 31.03.2016.
 */
interface ICommandFactory {
    fun createCommand(exec:CommandExecutor): CommandCallable?
}