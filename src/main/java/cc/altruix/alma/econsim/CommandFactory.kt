package cc.altruix.alma.econsim

import org.spongepowered.api.command.CommandCallable
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.command.spec.CommandSpec

/**
 * Created by pisarenko on 31.03.2016.
 */
class CommandFactory : ICommandFactory {
    override fun createCommand(exec: CommandExecutor): CommandCallable {
        return CommandSpec.builder().executor(exec).build()
    }
}