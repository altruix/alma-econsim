package cc.altruix.alma.econsim

import cc.altruix.alma.econsim.data.ImmutableSimData
import cc.altruix.alma.econsim.data.SimData
import cc.altruix.alma.econsim.data.SimDataManipulatorBuilder
import com.google.inject.Inject
import org.slf4j.Logger
import org.spongepowered.api.Game
import org.spongepowered.api.Sponge
import org.spongepowered.api.command.CommandCallable
import org.spongepowered.api.command.args.GenericArguments
import org.spongepowered.api.command.spec.CommandSpec
import org.spongepowered.api.event.Listener
import org.spongepowered.api.event.game.state.GameInitializationEvent
import org.spongepowered.api.event.game.state.GamePreInitializationEvent
import org.spongepowered.api.plugin.Plugin
import org.spongepowered.api.text.Text
import java.util.concurrent.TimeUnit

/**
 * Created by pisarenko on 29.03.2016.
 */
@Plugin(id = "alma-econsim", name = "Altruix Alma Econsim", version = "1.0")
open class AlmaEconSimPlugin(val cmdFactory:ICommandFactory = CommandFactory()) {
    @Inject lateinit var logger: Logger
    @Inject lateinit var game: Game

    @Listener
    fun onPreInit(event: GamePreInitializationEvent) {
        // Test this
        registerSimData()

        val gm = game
        if (gm == null) {
            return;
        }
        gm.commandManager.register(
                this,
                createAlmaTestCommand(),
                "almatest"
        )
        gm.commandManager.register(
                this,
                createSpawnMortal(),
                "spm"
        )
        gm.commandManager.register(
                this,
                createInitSimCommand(),
                "initSim"
        )
        gm.commandManager.register(
                this,
                createSimCommand(),
                "sim"
        )

    }

    protected fun registerSimData() {
        Sponge.getDataManager().register(SimData::class.java, ImmutableSimData::class.java, SimDataManipulatorBuilder())
        Sponge.getDataManager().registerBuilder(SimData::class.java, SimDataManipulatorBuilder())
    }

    open fun createInitSimCommand(): CommandCallable? {
        return cmdFactory.createCommand(InitSimCommand())
    }

    open fun createSimCommand(): CommandCallable? {
        return CommandSpec
                .builder()
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.integer(SimCommand.days.toMc()))
                )
                .executor(SimCommand())
                .build()

    }

    @Listener
    fun onGameInit(event: GameInitializationEvent){
        val scheduler = Sponge.getScheduler()
        val taskBuilder = scheduler.createTaskBuilder()
        val task = taskBuilder.execute(DeathTask(game))
                .async()
                .interval(1, TimeUnit.DAYS)
                .name("Death task")
                .submit(this);
    }
    private fun createSpawnMortal(): CommandCallable? {
        return CommandSpec
                .builder()
                .description(Text.of("Spawns a villager, which dies of hunger, if he or she doesn't eat enough"))
                .executor(SpawnMortalCommand()).build()
    }

    private fun createAlmaTestCommand(): CommandSpec {
        return CommandSpec
                .builder()
                .description(Text.of("Helps verify that Alma EconSim is installed and running"))
                .executor(AlmaTestCommand()).build()
    }
}

