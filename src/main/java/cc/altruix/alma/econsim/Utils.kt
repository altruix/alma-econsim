package cc.altruix.alma.econsim

import cc.altruix.alma.econsim.data.SimData
import cc.altruix.alma.econsim.data.SimKeys
import org.spongepowered.api.entity.Entity
import org.spongepowered.api.text.LiteralText
import org.spongepowered.api.text.Text
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World

/**
 * Created by pisarenko on 31.03.2016.
 */
fun String.toMc(): LiteralText {
    return Text.of(this)
}

fun Entity.setDaysSinceLastMeal(value:Int) {
    if (this.supports(SimKeys.DAYS_SINCE_LAST_MEAL)) {
        val simData = this.getOrCreate(SimData::class.java).get()
        simData.daysSinceLastMeal().set(value)
    }
}

fun Entity.setName(value:String) {
    if (this.supports(SimKeys.NAME)) {
        val simData = this.getOrCreate(SimData::class.java).get()
        simData.name().set(value)
    }
}

fun Entity.getName():String {
    if (this.supports(SimKeys.NAME)) {
        val simData = this.getOrCreate(SimData::class.java).get()
        simData.name().get()
    }
    return ""
}
fun Entity.getDaysSinceLastMeal():Int {
    if (this.supports(SimKeys.DAYS_SINCE_LAST_MEAL)) {
        val simData = this.getOrCreate(SimData::class.java).get()
        return simData.daysSinceLastMeal().get() ?: -1
    }
    return -1
}

fun World.findEntityByName(name:String):Entity? {
    return this.entities.filter { x -> name.equals(x.getName()) }.first()
}

fun createFoodLocation(src: Location<World>, world: World)= Location<World>(
        world,
        src.x + 2,
        src.y,
        src.z
)

fun Entity.incrementDaysSinceLastMeal() {
    if (this.supports(SimKeys.DAYS_SINCE_LAST_MEAL)) {
        val simData = this.getOrCreate(SimData::class.java).get()
        simData.daysSinceLastMeal().set(simData.daysSinceLastMeal().get() + 1)
    }
}
