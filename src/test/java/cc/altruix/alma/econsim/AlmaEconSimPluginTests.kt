package cc.altruix.alma.econsim

import org.fest.assertions.Assertions
import org.junit.Test
import org.mockito.Mockito
import org.slf4j.Logger
import org.spongepowered.api.Game
import org.spongepowered.api.command.CommandCallable
import org.spongepowered.api.command.CommandManager
import org.spongepowered.api.command.spec.CommandExecutor
import org.spongepowered.api.event.game.state.GameInitializationEvent
import org.spongepowered.api.event.game.state.GamePreInitializationEvent

/**
 * Created by pisarenko on 31.03.2016.
 */
class AlmaEconSimPluginTests {
    @Test
    fun onPreInitRegistersInitSimCommand() {
        val out = Mockito.spy(AlmaEconSimPlugin())
        out.game = Mockito.mock(Game::class.java)
        out.logger = Mockito.mock(Logger::class.java)
        val cmdMgr = Mockito.mock(CommandManager::class.java)
        val evt = Mockito.mock(GamePreInitializationEvent::class.java)
        Mockito.`when`(out.game.commandManager).thenReturn(cmdMgr)
        val initSimCommand = Mockito.mock(CommandCallable::class.java)
        Mockito.doReturn(initSimCommand).`when`(out).createInitSimCommand()

        // Run method under test
        out.onPreInit(evt)

        // Verify
        Mockito.verify(cmdMgr).register(out, initSimCommand, "initSim")
    }


    @Test
    fun createInitSimCommandSunnyDay() {
        val initSimCommand = Mockito.mock(CommandCallable::class.java)
        val cmdFactory = Mockito.spy(CommandFactoryForTesting(initSimCommand))
        val out = AlmaEconSimPlugin(cmdFactory)
        // Run method under test
        val res = out.createInitSimCommand()

        // Verify
        Assertions.assertThat(res).isSameAs(initSimCommand)
    }
}