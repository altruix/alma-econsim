package cc.altruix.alma.econsim

import org.fest.assertions.Assertions
import org.junit.Test
import org.mockito.Mockito
import org.spongepowered.api.entity.living.player.Player
import org.spongepowered.api.world.Location
import org.spongepowered.api.world.World

/**
 * Created by pisarenko on 31.03.2016.
 */
class InitSimCommandTests {
    @Test
    fun createVillager1LocationSunnyDay() {
        val src = Mockito.mock(Player::class.java)
        val world = Mockito.mock(World::class.java)
        Mockito.`when`(src.world).thenReturn(world)
        val location = Location<World>(world, 1, 2, 3)
        Mockito.`when`(src.location).thenReturn(location)
        val out = InitSimCommand()
        // Run method under test
        val res = out.createVillager1Location(src)
        // Verify
        Assertions.assertThat(res.x).isEqualTo(1.0)
        Assertions.assertThat(res.y).isEqualTo(2.0)
        Assertions.assertThat(res.z).isEqualTo(3.0)
    }
    @Test
    fun createVillager2LocationSunnyDay() {
        val src = Mockito.mock(Player::class.java)
        val world = Mockito.mock(World::class.java)
        Mockito.`when`(src.world).thenReturn(world)
        val location = Location<World>(world, 1, 2, 3)
        Mockito.`when`(src.location).thenReturn(location)
        val out = InitSimCommand()
        // Run method under test
        val res = out.createVillager2Location(src)
        // Verify
        Assertions.assertThat(res.x).isEqualTo(3.0)
        Assertions.assertThat(res.y).isEqualTo(2.0)
        Assertions.assertThat(res.z).isEqualTo(3.0)
    }
}