package cc.altruix.alma.econsim

import org.fest.assertions.Assertions
import org.spongepowered.api.command.CommandCallable
import org.spongepowered.api.command.spec.CommandExecutor
import java.lang.reflect.Type
import kotlin.reflect.KClass

/**
 * Created by pisarenko on 31.03.2016.
 */
open class CommandFactoryForTesting(val result:CommandCallable) : ICommandFactory {
    override fun createCommand(exec: CommandExecutor): CommandCallable? {
        Assertions.assertThat(exec).isNotNull
        Assertions.assertThat(exec).isInstanceOf(InitSimCommand::class.java)
        return result
    }
}